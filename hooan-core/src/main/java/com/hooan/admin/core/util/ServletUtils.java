package com.hooan.admin.core.util;

import org.springframework.web.context.request.RequestAttributes;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;
import com.hooan.admin.core.constant.HttpConstant;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.util.Objects;
import java.util.Optional;

/**
 * 修改记录
 * +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
 * 修改日期                 修改者                 备注信息
 * +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
 * 2023年03月24日                    phen hooan                    初始化版本
 */
public class ServletUtils {

    public static String getParameter(String name) {
        return Objects.requireNonNull(getRequest()).getParameter(name);
    }

    public static String getParameter(String name, String defaultValue) {
        return Optional.ofNullable(getParameter(name)).orElse(defaultValue);
    }

    public static String getRequestMethod(){
        return Objects.requireNonNull(getRequest()).getMethod();
    }

    public static String getRequestURI(){
        return Objects.requireNonNull(getRequest()).getRequestURI();
    }

    public static String getUserAgent(){
        return Objects.requireNonNull(getRequest()).getHeader(HttpConstant.Header.USER_AGENT);
    }

    public static ServletRequestAttributes getRequestAttributes() {
        try {
            RequestAttributes attributes = RequestContextHolder.getRequestAttributes();
            return (ServletRequestAttributes) attributes;
        } catch (Exception e) {
            return null;
        }
    }

    public static HttpServletRequest getRequest() {
        try {
            return Objects.requireNonNull(getRequestAttributes()).getRequest();
        } catch (Exception e) {
            return null;
        }
    }

    public static HttpServletResponse getResponse() {
        try {
            return Objects.requireNonNull(getRequestAttributes()).getResponse();
        } catch (Exception e) {
            return null;
        }
    }

    public static HttpSession getSession() {
        return Objects.requireNonNull(getRequest()).getSession();
    }

}
