package com.hooan.admin.core.util;

import cn.hutool.core.collection.CollectionUtil;
import cn.hutool.core.util.ArrayUtil;

import java.io.File;
import java.lang.annotation.Annotation;
import java.lang.reflect.Method;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.function.Predicate;

/**
 * 修改记录
 * +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
 * 修改日期                 修改者                 备注信息
 * +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
 * 2023年03月29日                    phen hooan                    初始化版本
 */
public class ClassScanner {

    public static List<Class<?>> scanClasses(String packageName, Predicate<Class<?>> predicate) {
        List<Class<?>> classes = new ArrayList<>();
        String packagePath = packageName.replace('.', '/');
        ClassLoader classLoader = Thread.currentThread().getContextClassLoader();
        try {
            URL packageUrl = classLoader.getResource(packagePath);
            if (packageUrl != null) {
                File packageDir = new File(packageUrl.toURI());
                if (packageDir.exists() && packageDir.isDirectory()) {
                    File[] files = packageDir.listFiles();
                    if (ArrayUtil.isEmpty(files)) return classes;
                    for (File file : files) {
                        String fileName = file.getName();
                        if (file.isFile() && fileName.endsWith(".class")) {
                            String className = packageName + '.' + fileName.substring(0, fileName.lastIndexOf('.'));
                            Class<?> clazz = classLoader.loadClass(className);
                            if (predicate.test(clazz)) {
                                classes.add(clazz);
                            }
                        } else if (file.isDirectory()) {
                            String subPackageName = packageName + '.' + fileName;
                            classes.addAll(scanClasses(subPackageName, predicate));
                        }
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return classes;
    }

    public static <T extends Annotation> List<T> findMethodAnnotation(String packageName, Class<T> annotation) {
        List<T> results = new ArrayList<>();
        List<Class<?>> classes = scanClasses(packageName, i -> true);
        if (CollectionUtil.isEmpty(classes)) return results;

        for (Class<?> clazz : classes) {
            Method[] methods = clazz.getMethods();
            if (ArrayUtil.isEmpty(methods)) continue;
            for (Method method : methods) {
                if (method.isAnnotationPresent(annotation)) {
                    results.add(method.getAnnotation(annotation));
                }
            }
        }
        return results;
    }

    public static <T extends Annotation> List<T> findMethodAnnotation(List<String> packageNames, Class<T> annotation) {
        List<T> results = new ArrayList<>();

        if (CollectionUtil.isEmpty(packageNames)) return results;
        for (String packageName : packageNames) {
            results.addAll(findMethodAnnotation(packageName, annotation));
        }
        return results;
    }
}