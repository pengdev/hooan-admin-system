package com.hooan.admin.core.constant

import com.baomidou.mybatisplus.annotation.IEnum
import com.fasterxml.jackson.annotation.JsonValue

interface IEnumCommon : IEnum<String> {
    val code: String get() = this.toString()

    val desc: String

    val type: String get() = "primary"

    val checked: Boolean get() = false

    @JsonValue
    override fun getValue(): String = code

    companion object {
        @JvmStatic
        fun <T> codeOf(clazz: Class<T> , code: String ) : T? {
            var constants = clazz.enumConstants
            if (constants != null) {
                for (constant in constants) {
                    if (constant is IEnumCommon && constant.code == code) run {
                        return constant
                    }
                }
            }
            return null
        }
    }
}