package com.hooan.admin.core.domain;

import com.alibaba.fastjson2.annotation.JSONField;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * 修改记录
 * +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
 * 修改日期                 修改者                 备注信息
 * +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
 * 2023年03月24日                    phen hooan                    初始化版本
 */
@Data
@JsonInclude(JsonInclude.Include.NON_EMPTY)
@Accessors(chain = true)
public class R<T> implements Serializable {
    private boolean ok;
    private int code;
    private String msg;
    private T data;
    @JSONField(name = "instance_id")
    private String instanceId;

    public R (boolean ok, int code, String msg, T data) {
        this.ok = ok;
        this.code = code;
        this.msg = msg;
        this.data = data;
    }

    public static <T> R<T> ok(){
        return new R<T>(true, 200,"", null);
    }

    public static <T> R<T> ok(T data){
        return new R<T>(true, 200, "", data);
    }

    public static <T> R<T> ok(String msg, T data){
        return new R<T>(true, 200, msg, data);
    }

    public static <T> R<T> fail(){
        return new R<T>(false, 500,"系统繁忙, 请稍后重试!", null);
    }

    public static <T> R<T> fail(String msg){
        return new R<T>(false,500, msg, null);
    }

    public static <T> R<T> fail(int code, String msg){
        return new R<T>(false, code, msg, null);
    }

    public static <T> R<T> fail(String msg, T data){
        return new R<T>(false,500,  msg, data);
    }
}
