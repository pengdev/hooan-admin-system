package com.hooan.admin.core.exception;

/**
 * 修改记录
 * +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
 * 修改日期                 修改者                 备注信息
 * +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
 * 2023年03月30日                    phen hooan                    初始化版本
 */
public class BusinessExceptionWrapper extends BusinessException{

    public static BusinessExceptionWrapper wrap(Throwable cause){
        return new BusinessExceptionWrapper(cause.getMessage(), cause);
    }

    public static BusinessExceptionWrapper wrap(String instanceId, Throwable cause){
        BusinessExceptionWrapper wrapper = new BusinessExceptionWrapper(cause.getMessage(), cause);
        wrapper.setInstanceId(instanceId);
        return wrapper;
    }

    public BusinessExceptionWrapper(String message) {
        super(message);
    }

    public BusinessExceptionWrapper(Throwable cause) {
        super(cause);
    }

    public BusinessExceptionWrapper(String message, Throwable cause) {
        super(message, cause);
    }
}
