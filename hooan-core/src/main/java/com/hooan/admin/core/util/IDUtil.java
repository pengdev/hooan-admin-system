package com.hooan.admin.core.util;

import cn.hutool.core.lang.UUID;

/**
 * 修改记录
 * +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
 * 修改日期                 修改者                 备注信息
 * +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
 * 2023年03月30日                    phen hooan                    初始化版本
 */
public class IDUtil {

    public static String getUUID(){
        return UUID.randomUUID().toString().replaceAll("-", "");
    }
}
