package com.hooan.admin.core.exception;

import cn.hutool.core.collection.CollectionUtil;
import cn.hutool.core.util.StrUtil;

import java.util.Collection;
import java.util.Objects;

/**
 * 修改记录
 * +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
 * 修改日期                 修改者                 备注信息
 * +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
 * 2023年03月29日                    phen hooan                    初始化版本
 */
public class Exceptions {
    public static void isNull(Object obj, String msg) {
        if (Objects.isNull(obj)) {
            throw new BusinessException(msg);
        }
    }

    public static void empty(String str, String msg) {
        if (StrUtil.isEmpty(str)) {
            throw new BusinessException(msg);
        }
    }

    public static void empty(Collection<?> collection, String msg) {
        if (CollectionUtil.isEmpty(collection)) {
            throw new BusinessException(msg);
        }
    }

    public static void equals(String arg1, String arg2, String msg) {
        if(StrUtil.equals(arg1, arg2)) {
            throw new BusinessException(msg);
        }
    }

    public static void notEquals(String arg1, String arg2, String msg) {
        if(! StrUtil.equals(arg1, arg2)) {
            throw new BusinessException(msg);
        }
    }

    public static void isTrue(boolean condition, String msg) {
        if (condition) {
            throw new BusinessException(msg);
        }
    }

    public static void isFalse(boolean condition, String msg){
        if (! condition) {
            throw new BusinessException(msg);
        }
    }
}
