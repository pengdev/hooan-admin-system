package com.hooan.admin.core.constant;

/**
 * 修改记录
 * +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
 * 修改日期                 修改者                 备注信息
 * +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
 * 2023年03月30日                    phen hooan                    初始化版本
 */
public interface HttpConstant {

    interface Header {
        String USER_AGENT = "User-Agent";
    }
}
