package com.hooan.admin.core.constant

class Enums {
    enum class Status(override val code: String, override val desc: String, override val type: String, override val checked: Boolean) :
        IEnumCommon {
        NORMAL("1", "正常", "primary", true),
        DISABLE("0", "禁用", "warning", false)
    }

    enum class BasicStatus(override val code: String, override val desc: String, override val type: String, override val checked: Boolean) :
        IEnumCommon {
        ACTIVE("A", "启用", "primary", true),
        INACTIVE("I", "禁用", "warning", false)
    }

    enum class Deleted(override val code: String, override val desc: String, override val type: String, override val checked: Boolean) :
        IEnumCommon {
        NO("0", "未删除", "primary", true),
        YES("1", "已删除", "danger", false)
    }

    enum class YesOrNo(override val code: String, override val desc: String, override val type: String) : IEnumCommon {
        YES("1", "是", "primary"),
        NO("0", "否", "danger")
    }

    enum class CharYesOrNo(override val code: String, override val desc: String, override val type: String) :
        IEnumCommon {
        YES("Y", "是", "primary"),
        NO("N", "否", "danger")
    }

    enum class Sex(override val code: String, override val desc: String) : IEnumCommon {
        UNKNOWN("0", "未知"),
        MALE("1", "男"),
        FEMALE("2", "女");
    }
    enum class LoginStatus(override val code: String, override val desc: String, override val type: String) :
        IEnumCommon {
        ONLINE("0", "在线", "primary"),
        OFFLINE("1", "下线", "info"),
        FORCE_OFFLINE("2", "强制下线", "info"),
        EXPIRE("3", "过期", "warning"),
        KICK_OUT("4", "踢出", "danger"),
    }

    enum class MenuVisible(override val code: String, override val desc: String, override val type: String) :
        IEnumCommon {
        SHOW("1", "显示", "primary"),
        BLANK("0", "隐藏", "warning")
    }

    enum class MenuType(override val code: String, override val desc: String, override val type: String) : IEnumCommon {
        DIRECTORY("M", "目录", "primary"),
        MENU("C", "菜单", "success"),
        BUTTON("F", "按钮", "warning")
    }

    enum class ComponentType(override val code: String, override val desc: String, override val checked: Boolean) :
        IEnumCommon {
        GROUP("GROUP", "配置组", true),
        INPUT("INPUT", "输入框", false),
        RADIO("RADIO", "单选框", false),
        IMAGE("IMAGE", "图片", false),
        COLOR_PICKER("COLOR_PICKER", "颜色选择器", false),
        PIXEL_INPUT("PIXEL_INPUT", "像素输入框", false),
    }

    enum class ArticleStatus(override val code: String, override val desc: String, override val type: String) :
        IEnumCommon {
        NEW("N", "新建", "info"),
        SYNC("S", "同步", "success"),
        FINISH("F", "完成", "primary"),
        DRAFT("D", "草稿", "success"),
    }

    enum class LoadStatus(override val code: String, override val desc: String, override val type: String) :
        IEnumCommon {
        LOADED("L", "已加载", "primary"),
        UNLOAD("U", "未加载", "info"),
    }

    enum class FreshStatus(override val code: String, override val desc: String, override val type: String) :
        IEnumCommon {
        NEW("N", "无更新", "success"),
        OLD("O", "有更新", "warning"),
    }

    enum class ClientPlatform(override val code: String, override val desc: String, override val type: String) :
        IEnumCommon {
        WECHAT_MP("wechat_mp", "微信小程序", "success"),
        WEB_H5("web_h5", "网页H5", "success"),
    }

    enum class TransType(override val code: String, override val desc: String, override val type: String) :
        IEnumCommon {
        TRANSACTION("T", "交易", "success"),
        REFUND("R", "退款", "danger"),
    }

    enum class OrderStatus(override val code: String, override val desc: String, override val type: String) :
        IEnumCommon {
        NO_PAY("no_pay", "未支付", "warning"),
        PAID("paid", "支付完成", "success"),
        REFUNDING("refunding", "退款中", "warning"),
        REFUNDED("refunded", "已退款", "danger"),
        COMPLETE("complete", "交易完成", "primary"),
    }

    enum class OrderPayType(override val code: String, override val desc: String, override val type: String) :
        IEnumCommon {
        WX_PAY("wxpay", "微信支付", "success"),
    }

    enum class OrderDetailType(override val code: String, override val desc: String, override val type: String) :
        IEnumCommon {
        MEMBER("member", "会员套餐", "success"),
    }

    enum class ReportStatisticalTime(override val code: String, override val desc: String, override val type: String) :
        IEnumCommon {
        TODAY("today", "今日", "success"),
        YESTERDAY("yesterday", "昨日", "success"),
        WEEK("week", "本周", "success"),
        MONTH("month", "本月", "success"),
    }
}