package com.hooan.admin.core.exception;

/**
 * 修改记录
 * +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
 * 修改日期                 修改者                 备注信息
 * +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
 * 2023年03月24日                    phen hooan                    初始化版本
 */
public interface ExceptionExecutor {

    void doSomething() throws Throwable;

    static void execute(ExceptionExecutor wrapper){
        try {
            wrapper.doSomething();
        } catch (Throwable e) {
            throw new BusinessException(e.getMessage(), e);
        }
    }
}
