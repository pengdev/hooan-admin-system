package com.hooan.admin.core.util;

import cn.hutool.core.collection.CollectionUtil;

import java.util.List;
import java.util.function.Function;
import java.util.function.Predicate;
/**
 * 修改记录
 * +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
 * 修改日期                 修改者                 备注信息
 * +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
 * 2023年04月17日                    phen hooan                    初始化版本
 */
public class RecursiveUtil {
    /**
     * @param roots 根节点
     * @param handler 单个节点查询孩子元素处理逻辑
     * @param isGo 是否继续
     * @param <T> 节点类型
     */
    public static <T> void recursive(List<T> roots, Function<T, List<T>> handler, Predicate<T> isGo){
        if (CollectionUtil.isEmpty(roots)){
            return;
        }
        for (T t : roots) {
            if (! isGo.test(t)){
                continue;
            }
            List<T> nodes = handler.apply(t);
            recursive(nodes, handler, isGo);
        }
    }

    public static <T> void recursive(List<T> roots, Function<T, List<T>> handler){
        recursive(roots, handler, t->true);
    }

    /**
     * 递归查询子元素
     * @param roots 根节点列表
     * @param pidGain 获取查询子元素的父ID
     * @param childSetter 设置孩子元素
     * @param dataGain 获取孩子元素的方法
     * @param isGo 是否继续递归
     * @param <R> 节点ID, 父节点ID类型
     * @param <T> 节点类型
     */
    public static  <R, T> void recursive(List<? extends T> roots, Function<T, R> pidGain, ChildSetMethod<T> childSetter, Function<R, List<T>> dataGain, Predicate<T> isGo){
        if (CollectionUtil.isEmpty(roots)){
            return;
        }
        for (T t : roots) {
            if (! isGo.test(t)){
                continue;
            }
            R parentId = pidGain.apply(t);
            List<T> nodes = dataGain.apply(parentId);
            childSetter.setChildren(t, nodes);
            recursive(nodes, pidGain, childSetter, dataGain, isGo);
        }
    }

    /**
     * 递归查询子元素
     * @param roots 根节点列表
     * @param pidGain 获取查询子元素的父ID
     * @param childSetter 设置孩子元素
     * @param dataGain 获取孩子元素的方法
     * @param <R> 节点ID, 父节点ID类型
     * @param <T> 节点类型
     */
    public static  <R, T> void recursive(List<? extends T> roots, Function<T, R> pidGain, ChildSetMethod<T> childSetter, Function<R, List<T>> dataGain){
        recursive(roots, pidGain, childSetter, dataGain, t -> true);
    }


    public interface ChildSetMethod<T>{
        void setChildren(T e, List<T> c);
    }
}
