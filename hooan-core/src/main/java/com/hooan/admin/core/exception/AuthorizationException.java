package com.hooan.admin.core.exception;

/**
 * 修改记录
 * +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
 * 修改日期                 修改者                 备注信息
 * +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
 * 2023年03月29日                    phen hooan                    初始化版本
 */
public class AuthorizationException extends BusinessException{
    public AuthorizationException(String message) {
        super(message);
    }
}
