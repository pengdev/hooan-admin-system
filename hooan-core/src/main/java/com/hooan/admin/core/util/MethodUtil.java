package com.hooan.admin.core.util;

import java.lang.reflect.Method;

/**
 * 修改记录
 * +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
 * 修改日期                 修改者                 备注信息
 * +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
 * 2023年03月30日                    phen hooan                    初始化版本
 */
public class MethodUtil {

    public static String getFullMethodName(Method method){
        String className = method.getDeclaringClass().getName();
        String methodName = method.getName();
        return String.format("%s.%s()", className, methodName);
    }

    public static void main(String[] args) throws NoSuchMethodException {
        MethodUtil.getFullMethodName(MethodUtil.class.getDeclaredMethod("getFullMethodName", Method.class));
    }
}
