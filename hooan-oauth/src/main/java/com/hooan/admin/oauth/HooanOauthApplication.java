package com.hooan.admin.oauth;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class HooanOauthApplication {

	public static void main(String[] args) {
		SpringApplication.run(HooanOauthApplication.class, args);
	}

}
