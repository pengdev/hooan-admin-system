package com.hooan.admin.oauth.security;

import com.alibaba.fastjson2.JSONObject;
import com.hooan.admin.core.domain.R;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.AuthenticationEntryPoint;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

/**
 * 修改记录
 * +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
 * 修改日期                 修改者                 备注信息
 * +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
 * 2024年03月27日                    phen hooan                    初始化版本
 */
public class HooanAuthenticationEntryPoint implements AuthenticationEntryPoint {

    @Override
    public void commence(HttpServletRequest request, HttpServletResponse response, AuthenticationException exception) throws IOException, ServletException {
        PrintWriter writer = response.getWriter();
        writer.write(JSONObject.toJSONString(R.fail(401, exception.getMessage())));
        writer.flush();
        writer.close();
    }
}
