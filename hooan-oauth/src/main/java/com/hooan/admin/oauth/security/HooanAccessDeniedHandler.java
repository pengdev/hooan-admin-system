package com.hooan.admin.oauth.security;

import com.alibaba.fastjson2.JSONObject;
import com.hooan.admin.core.domain.R;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.web.access.AccessDeniedHandler;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

/**
 * 修改记录
 * +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
 * 修改日期                 修改者                 备注信息
 * +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
 * 2024年03月27日                    phen hooan                    初始化版本
 */
public class HooanAccessDeniedHandler implements AccessDeniedHandler {
    @Override
    public void handle(HttpServletRequest request, HttpServletResponse response, AccessDeniedException exception) throws IOException, ServletException {
        PrintWriter writer = response.getWriter();
        writer.write(JSONObject.toJSONString(R.fail(403, exception.getMessage())));
        writer.flush();
        writer.close();
    }
}
